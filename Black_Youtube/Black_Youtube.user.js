// ==UserScript==
// @name Black Youtube
// @namespace MysteriousDruid
// @description Make all youtube pages without "/watch" in the url have a background of #000
// @include http*://*youtube.com/*
// @require http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.js
// @exclude http*://*youtube.com/watch*
// @exclude http*://*youtube.com/atch*
// @version 0.0.1
// @grant none
// ==/UserScript==



$(function(){

document.body.style.backgroundColor="#000";
$('#yt-masthead-container').css("background-color","#000");
$('#yt-masthead-container').css("border-bottom", "1px solid #333");
$('#footer-container').css("background-color", "#000");
$('#footer-container').css("border-top", "1px solid #333");
$('#masthead-search-term').css("background-color", "#333");
$('.masthead-search-terms-border').css("border-color", "#333");
$('.masthead-search-terms-border').css("box-shadow", "0px 1px 2px #333 inset");
$('.masthead-search-terms-border').css("background-color", "#333");
$('#search-btn').css("background-color", "#333");
$('#search-btn').css("border-color","#333");
$('.yt-uix-button').css("background-color", "#333");
$('.yt-uix-button-default').css("color", "#000");
$('.yt-uix-button-default').css("border-color", "#333");
$('.yt-lockup-playlist-item').css("border-bottom", "#333");
$('.yt-uix-button-subscribe-unbranded').css("background-image", "linear-gradient(to top, #333 0px, #333 100%)");
$('.yt-uix-button-subscribe-unbranded').css("color", "#000");
$('.yt-uix-button-subscribe-unbranded').css("border-color", "#000");
$('#yt-picker-language-button').css("border-color", "#000");
$('#yt-picker-country-button').css("border-color", "#000");
$('#yt-picker-safetymode-button').css("border-color", "#000");
$('#google-help').css("border-color", "#000");
$('.guide-section-separator').css("border-bottom", "1px solid #333");
$('.branded-page-v2-primary-col').css("background-color","#000");
$('.branded-page-v2-primary-col').css("border-right","1px solid #333");
$('.branded-page-v2-primary-col').css("border-left","1px solid #333");
$('.branded-page-v2-primary-col').css("border-top","1px solid #333");
$('.branded-page-v2-primary-col').css("border-bottom","1px solid #333");
$('.feed-item-container .feed-item-main').css("border-bottom", "1px solid #333");
$('.compact-shelf .yt-uix-button-shelf-slider-pager ').css("background-color","#333");
$('#masthead-expanded-container').css("background-color", "#000");
$('#masthead-expanded-container').css("border-bottom", "1px solid #333");
$('.yt-lockup-title a').css("color", "#333");
$('.yt-user-name').css("color", "#333");
$('#footer-main').css("border-bottom","1px solid #333");
$('.multirow-shelf-expander').css("color", "#333");
$('.search-header').css("border-bottom", "1px solid #333");
$('.yt-badge').css("border", "1px solid #333");

document.body.onkeydown=function()
{
$('.gssb_m').css('background-color',"#333");
};




});

